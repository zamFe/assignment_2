﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetPage(int limit, int offset);
        T GetById(int Id);
        void Add(T entity);
        void Delete(T entity);
        void Edit(T entity);


    }
}
