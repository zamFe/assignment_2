﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlTypes;

namespace Assignment_2
{
    public class CustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Adds a customer to the database
        /// </summary>
        /// <param name="entity"></param>
        public void Add(Customer entity)
        {
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = "INSERT INTO Customer " +
                                 "VALUES(@FirstName,@LastName,@Company," +
                                 "@Adress,@City,@State,@Country,@PostalCode," +
                                 "@Phone,@Fax,@Email,@SupportRepId)";

                    using (SqlCommand command = new(sql, connection))
                    {
                        Console.WriteLine(command.CommandText);
                        command.ExecuteNonQuery();

                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Deletes a customer from the database. NOTE: Does not account for key-relationships
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(Customer entity)
        {
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = "DELETE FROM Customer " +
                                 $"WHERE CustomerId = {entity.CustomerId}";

                    using (SqlCommand command = new(sql, connection))
                    {
                        AddAllValues(command, entity);
                        

                        Console.WriteLine(command.CommandText);
                        command.ExecuteNonQuery();

                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Updates the values of a customer in the database
        /// </summary>
        /// <param name="entity"></param>
        public void Edit(Customer entity)
        {
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = "UPDATE Customer " +
                                 "SET " +
                                 "FirstName = @FirstName," +
                                 "LastName = @LastName," +
                                 "Company = @Company," +
                                 "Address = @Address," +
                                 "City = @City," +
                                 "State = @State," +
                                 "Country = @Country," +
                                 "PostalCode = @PostalCode," +
                                 "Phone = @Phone," +
                                 "Fax = @Fax," +
                                 "Email = @Email," +
                                 "SupportRepId = @SupportRepId " +
                                  $"WHERE CustomerId = {entity.CustomerId} ";

                    using (SqlCommand command = new(sql, connection))
                    {
                        AddAllValues(command, entity);

                        Console.WriteLine(command.CommandText);
                        command.ExecuteNonQuery();

                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Returns a list of all the customers in the database
        /// </summary>
        /// <returns>List of Customer</returns>
        public IEnumerable<Customer> GetAll()
        {
            List<Customer> output = new();
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = "SELECT * FROM Customer";

                    using (SqlCommand command = new(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                output.Add(ReadCustomer(reader));
                            }


                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return output;
        }

        /// <summary>
        /// Returns a specific customer by Id from the database
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Customer object</returns>
        public Customer GetById(int Id)
        {
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = $"SELECT * FROM Customer WHERE CustomerId = {Id}";

                    using (SqlCommand command = new(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            reader.Read();
                            return ReadCustomer(reader);
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            throw new Exception("Data not found");
        }

        /// <summary>
        /// Returns a specific customer by Name from the database
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Customer object</returns>
        public Customer GetByName(String name)
        {
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = $"SELECT * FROM Customer WHERE FirstName LIKE '%{name}%'";

                    using (SqlCommand command = new(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            reader.Read();
                            return ReadCustomer(reader);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            throw new Exception("Data not found");
        }

        /// <summary>
        /// Returns a page of customers with specified limit and offset
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>list of Customer</returns>
        public IEnumerable<Customer> GetPage(int limit, int offset)
        {
            List<Customer> output = new();
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = "SELECT * FROM Customer " +
                                 $"WHERE CustomerId > {offset} " +
                                 $"AND CustomerId < {limit + offset + 1}";

                    using (SqlCommand command = new(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                output.Add(ReadCustomer(reader));
                            }


                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            return output;
        }

        /// <summary>
        /// Returns customer count per country from the database
        /// </summary>
        /// <returns>list of CustomerCountry</returns>
        public IEnumerable<CustomerCountry> GetCustomerCountries()
        {
            List<CustomerCountry> output = new();
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = "SELECT Country, COUNT(*) as Customers FROM Customer " +
                        "GROUP BY Country " +
                        "ORDER BY Customers DESC;";

                    using (SqlCommand command = new(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry cc = new
                                    (
                                    SqlManager.SafeGetString(reader, 0),
                                    SqlManager.SafeGetInt(reader, 1)
                                    );
                                output.Add(cc);

                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return output;
        }

        /// <summary>
        /// Requests total spendings and coresponding name for every customer in the database
        /// </summary>
        /// <returns>returns list of CustomerSpender</returns>
        public IEnumerable<CustomerSpender> GetCustomerSpendings()
        {
            List<CustomerSpender> output = new();
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = "SELECT FirstName, SUM(total) as Spendings FROM Customer " +
                        "INNER JOIN Invoice " +
                        "ON Customer.CustomerId = Invoice.CustomerId " +
                        "GROUP BY Customer.FirstName " +
                        "ORDER BY Spendings DESC";

                    using (SqlCommand command = new(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender cs = new
                                    (
                                    SqlManager.SafeGetString(reader, 0),
                                    SqlManager.SafeGetDecimal(reader, 1)
                                    );
                                output.Add(cs);

                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return output;
        }

        /// <summary>
        /// Requests favourite genre for every customer Id in the database
        /// </summary>
        /// <returns>list of CustomerGenre</returns>
        public IEnumerable<CustomerGenre> GetCustomerGenre()
        {
            List<CustomerGenre> output = new();
            try
            {
                using (SqlConnection connection = new(SqlManager.getConnectionString()))
                {
                    connection.Open();

                    Console.WriteLine("done. /n");

                    string sql = "SELECT firstTable.CustomerId, firstTable.Name " +
                        "FROM(SELECT Customer.CustomerId, COUNT(Genre.Name) as genreCount, Genre.Name FROM Customer " +
                        "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                        "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                        "INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId " +
                        "INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                        "GROUP BY Customer.CustomerId, Genre.Name) AS firstTable " +
                        "INNER JOIN " +
                        "(SELECT CustomerID, MAX(genreCount) as Gcc FROM(SELECT Customer.CustomerId, COUNT(Genre.Name) as genreCount, Genre.Name FROM Customer " +
                        "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                        "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                        "INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId " +
                        "INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                        "GROUP BY Customer.CustomerId, Genre.Name) AS gCountTable " +
                        "GROUP BY CustomerID) AS secondTable " +
                        "ON firstTable.CustomerId = secondTable.CustomerId " +
                        "AND firstTable.genreCount = secondTable.Gcc " +
                        "ORDER BY firstTable.CustomerId ASC";

                    using (SqlCommand command = new(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre cg = new
                                    (
                                    SqlManager.SafeGetInt(reader, 0),
                                    SqlManager.SafeGetString(reader, 1)
                                    );
                                output.Add(cg);

                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return output;
        }

        /// <summary>
        /// Reads all attributes of a customer from a sql data reader
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private Customer ReadCustomer(SqlDataReader reader)

        {
            return new Customer(SqlManager.SafeGetInt(reader, 0),
                                                    SqlManager.SafeGetString(reader, 1),
                                                    SqlManager.SafeGetString(reader, 2),
                                                    SqlManager.SafeGetString(reader, 3),
                                                    SqlManager.SafeGetString(reader, 4),
                                                    SqlManager.SafeGetString(reader, 5),
                                                    SqlManager.SafeGetString(reader, 6),
                                                    SqlManager.SafeGetString(reader, 7),
                                                    SqlManager.SafeGetString(reader, 8),
                                                    SqlManager.SafeGetString(reader, 9),
                                                    SqlManager.SafeGetString(reader, 10),
                                                    SqlManager.SafeGetString(reader, 11),
                                                    SqlManager.SafeGetInt(reader, 12));
        }

        /// <summary>
        /// Adds all values from a sql command to a customer entity
        /// </summary>
        /// <param name="command"></param>
        /// <param name="entity"></param>
        private void AddAllValues(SqlCommand command, Customer entity)
        {
            command.Parameters.AddWithValue("@FirstName", entity.FirstName ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@LastName", entity.LastName ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Company", entity.Company ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Address", entity.Address ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@City", entity.City ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@State", entity.State ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Country", entity.Country ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@PostalCode", entity.PostalCode ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Phone", entity.Phone ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Fax", entity.Fax ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@Email", entity.Email ?? (object)DBNull.Value);
            command.Parameters.AddWithValue("@SupportRepId", entity.SupportRepId);
        }

    }
}
