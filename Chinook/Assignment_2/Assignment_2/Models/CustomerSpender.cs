﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    public class CustomerSpender
    {
        public string Name { get; set; }
        public decimal Spendings { get; set; }

        public CustomerSpender(string name, decimal spendings)
        {
            Name = name;
            Spendings = spendings;
        }
    }
}
