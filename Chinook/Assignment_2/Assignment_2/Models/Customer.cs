﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public int SupportRepId { get; set; }

        public string Phone { get; set; }

        public Customer(int customerId, string firstName, string lastName, 
            string company, string address, string city, 
            string state, string country, string postalCode, string phone, 
            string fax, string email, int supportRepId)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
            Company = company;
            Address = address;
            City = city;
            State = state;
            Country = country;
            PostalCode = postalCode;
            Fax = fax;
            Email = email;
            SupportRepId = supportRepId;
            Phone = phone;
        }

        public Customer(string firstName , string lastName, string email,int supportRepId)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            SupportRepId = supportRepId;
        }

    }
}
