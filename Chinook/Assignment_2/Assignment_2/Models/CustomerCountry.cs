﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int Customers { get; set; }

        public CustomerCountry(string country, int customers)
        {
            Country = country;
            Customers = customers;
        }
    }
}
