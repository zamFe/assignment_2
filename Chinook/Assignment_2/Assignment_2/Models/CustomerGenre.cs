﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string FavouriteGenre { get; set; }

        public CustomerGenre(int customerId, string favouriteGenre)
        {
            CustomerId = customerId;
            FavouriteGenre = favouriteGenre;
        }
    }
}
