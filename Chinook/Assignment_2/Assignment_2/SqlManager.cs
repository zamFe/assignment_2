﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace Assignment_2
{
    public static class SqlManager
    {
        /// <summary>
        /// Returns connection string for sql server
        /// </summary>
        /// <returns></returns>
        public static string getConnectionString()
        {
            SqlConnectionStringBuilder builder = new();
            builder.DataSource = @"ND-5CG92747K5\SQLEXPRESS";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;

            return builder.ConnectionString;
        }

        /// <summary>
        /// Chekcs if column value is null before reading from database
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="colIndex"></param>
        /// <returns>string</returns>
        public static string SafeGetString(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }

        /// <summary>
        /// Chekcs if column value is null before reading from database
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="colIndex"></param>
        /// <returns>int</returns>
        public static int SafeGetInt(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetInt32(colIndex);
            return -1;
        }

        /// <summary>
        /// Chekcs if column value is null before reading from database
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="colIndex"></param>
        /// <returns>decimal</returns>
        public static decimal SafeGetDecimal(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetDecimal(colIndex);
            return -1;
        }
    }
}
