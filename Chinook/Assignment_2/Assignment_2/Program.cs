﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Assignment_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test get all
            CustomerRepository test = new();
            IEnumerable<Customer> testList = test.GetAll();

            foreach(Customer cust in testList)
            {
                Console.WriteLine($"Customer: {cust.CustomerId}     Name: {cust.FirstName} {cust.LastName}");
            }

            //Test get by Id
            Console.WriteLine();
            try { Console.WriteLine(test.GetById(5).FirstName); } 
            catch (Exception e) { Console.WriteLine(e.Message); }

            //Test get by Name
            try { Console.WriteLine(test.GetByName("Bjørn").Country); }
            catch (Exception e) { Console.WriteLine(e.Message); }

           //Test Edit
            Customer sinan = new("Sinan", "Karagülle", "sinan2009@hotmail.com",4);
            sinan.CustomerId = 60;

            //test.Edit(sinan);
        }
    }
}
