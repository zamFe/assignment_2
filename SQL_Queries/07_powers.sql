USE SuperheroesDb;

INSERT INTO dbo.Power
VALUES 
('Feline jump','Jumps high and graciously like a majestic cat'),
('Threatening bark', 'Barks furiously towards an opponent to scare him/her'),
('Defensive curl', 'Curls into a small ball to protect vital organs'),
('Caffeine boost', 'Drinks a caffeniated beverage, like pepsi, to gain a burst of energy');

INSERT INTO dbo.SuperheroPower
VALUES
(1,1),
(2,2),
(2,3),
(3,4),
(1,4);

