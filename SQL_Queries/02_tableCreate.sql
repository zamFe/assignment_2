USE SuperheroesDb;
CREATE TABLE Superhero (
	Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Name varchar(50),
	Alias varchar(50),
	Origin varchar(50)
);
CREATE TABLE Assistant (
	Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Name varchar(50) UNIQUE
);
CREATE TABLE Power (
	Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Name varchar(50),
	Description varchar(150)
);