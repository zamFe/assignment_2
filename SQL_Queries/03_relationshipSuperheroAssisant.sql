USE SuperheroesDb;
ALTER TABLE dbo.Assistant
ADD Superhero_Id int, 
CONSTRAINT fk_SuperHero
FOREIGN KEY (Superhero_Id) REFERENCES dbo.Superhero(Id);