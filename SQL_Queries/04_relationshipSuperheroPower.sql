USE SuperheroesDb;
CREATE TABLE SuperheroPower (
	Superhero_Id int,
	Power_Id int
	CONSTRAINT pk_superheroPower PRIMARY KEY (Superhero_id,Power_id),
	CONSTRAINT fk_superHeroPower_superhero
		FOREIGN KEY (Superhero_Id)
		REFERENCES dbo.Superhero,
	CONSTRAINT fk_superHeroPower_power
		FOREIGN KEY (Power_Id)
		REFERENCES dbo.Power
);